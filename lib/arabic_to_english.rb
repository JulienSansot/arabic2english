class ArabicToEnglish

  # these numbers can't be formed from other numbers
  # and they are the base for generating all other numbers
  BASE_NUMBERS = {
    "0" => "",
    "1" => "one",
    "2" => "two",
    "3" => "three",
    "4" => "four",
    "5" => "five",
    "6" => "six",
    "7" => "seven",
    "8" => "eight",
    "9" => "nine",
    "10" => "ten",
    "11" => "eleven",
    "12" => "twelve",
    "13" => "thirteen",
    "14" => "fourteen",
    "15" => "fifteen",
    "16" => "sixteen",
    "17" => "seventeen",
    "18" => "eighteen",
    "19" => "nineteen",
    "20" => "twenty",
    "30" => "thirty",
    "40" => "forty",
    "50" => "fifty",
    "60" => "sixty",
    "70" => "seventy",
    "80" => "eighty",
    "90" => "ninety"
  }

  # Translate an arabic number (like 1, 2 ,300)
  # to a number in english (like one, two, tree hundred).
  # - works with numbers with commas (like 1,000,000)
  # - maximum number that can be translated is (10^18)-1
  # - returns 0 if input is nil
  # - returns 0 if input can't be parsed
  def self.translate(arabic_number_str)
    
    arabic_number_str = (arabic_number_str || '') # if nil, it becomes ''

    # remove commas to be able to accept numbers like 123,456
    arabic_number_str = arabic_number_str.gsub(',','') 

    arabic_number = arabic_number_str.to_i

    if arabic_number == 0
      return "zero"
    end

    # transform to absolute value, we'll take care of the sign at the end
    arabic_number_abs = arabic_number.abs

    remaining = arabic_number_abs

    number_parts = [
      { literal: 'quadrillion', number: 1_000_000_000_000_000 },
      { literal: 'trillion',    number: 1_000_000_000_000 },
      { literal: 'billion',     number: 1_000_000_000 },
      { literal: 'million',     number: 1_000_000 },
      { literal: 'thousand',    number: 1_000 }
    ]

    result = ''
    number_parts.each{|number_part|
      part = remaining / number_part[:number]
      if part > 0
        result += translate_hundreds(part) + " #{number_part[:literal]} "
      end
      #removes the part we just translated
      remaining = remaining % number_part[:number]
    }

    # we have to add "and" for cases like "one thousands and one"
    if arabic_number_abs >= 1000 && remaining < 100 && remaining > 0
      result += 'and '
    end

    result += translate_hundreds(remaining)

    if arabic_number < 0
      result = "minus #{result}"
    end

    result.strip

  end



  private

    def self.translate_hundreds arabic_number

      result = ''

      hundreds = arabic_number / 100

      if hundreds > 0

        result += BASE_NUMBERS[hundreds.to_s].to_s
        result += ' hundred'

      end

      remaining = arabic_number % 100

      if remaining > 0
        if hundreds > 0
          result += ' and '
        end
        if remaining < 21
          result += BASE_NUMBERS[remaining.to_s].to_s
        else
          tenth = remaining / 10 * 10
          result += BASE_NUMBERS[tenth.to_s].to_s
          ones = remaining % 10
          if ones > 0
            result += ' ' + BASE_NUMBERS[ones.to_s].to_s
          end
        end
      end

      result

    end

end
