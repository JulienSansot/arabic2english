require 'arabic_to_english'

RSpec.describe ArabicToEnglish, "#translate" do

  context "nil" do
    it "should return 0" do
      result = ArabicToEnglish.translate(nil)
      expect(result).to eq "zero"
    end
  end

  context "an empty string" do
    it "should return 0" do
      result = ArabicToEnglish.translate("")
      expect(result).to eq "zero"
    end
  end

  context "a string which is not a number" do
    it "should return 0" do
      result = ArabicToEnglish.translate("すごい")
      expect(result).to eq "zero"
    end
  end

  context "a number with commas" do
    it "should return the right number" do
      result = ArabicToEnglish.translate("1,200")
      expect(result).to eq "one thousand two hundred"
    end
  end

  context "a negative number" do
    it "should return the right negative number" do
      result = ArabicToEnglish.translate("-20")
      expect(result).to eq "minus twenty"
    end
  end


  # testing numbers that can't be formed with other numbers
  simple_numbers = [
    {_in: "0", _out: "zero"},
    {_in: "1", _out: "one"},
    {_in: "2", _out: "two"},
    {_in: "3", _out: "three"},
    {_in: "4", _out: "four"},
    {_in: "5", _out: "five"},
    {_in: "6", _out: "six"},
    {_in: "7", _out: "seven"},
    {_in: "8", _out: "eight"},
    {_in: "9", _out: "nine"},
    {_in: "10", _out: "ten"},
    {_in: "11", _out: "eleven"},
    {_in: "12", _out: "twelve"},
    {_in: "13", _out: "thirteen"},
    {_in: "14", _out: "fourteen"},
    {_in: "15", _out: "fifteen"},
    {_in: "16", _out: "sixteen"},
    {_in: "17", _out: "seventeen"},
    {_in: "18", _out: "eighteen"},
    {_in: "19", _out: "nineteen"},
    {_in: "20", _out: "twenty"},
    {_in: "30", _out: "thirty"},
    {_in: "40", _out: "forty"},
    {_in: "50", _out: "fifty"},
    {_in: "60", _out: "sixty"},
    {_in: "70", _out: "seventy"},
    {_in: "80", _out: "eighty"},
    {_in: "90", _out: "ninety"}
  ]
  simple_numbers.each{|simple_number|

    context "#{simple_number[:_in]}" do
      it "should return #{simple_number[:_out]}" do
        result = ArabicToEnglish.translate(simple_number[:_in])
        expect(result).to eq simple_number[:_out]
      end
    end

  }


  # testing numbers that can be formed from other numbers
  test_numbers = [
    {_in: "34", _out: "thirty four"},
    {_in: "88", _out: "eighty eight"},
    {_in: "300", _out: "three hundred"},
    {_in: "1000", _out: "one thousand"},
    {_in: "2234", _out: "two thousand two hundred and thirty four"},
    {_in: "3300", _out: "three thousand three hundred"},
    {_in: "4412", _out: "four thousand four hundred and twelve"},
    {_in: "123123", _out: "one hundred and twenty three thousand one hundred and twenty three"},
    {_in: "1000000", _out: "one million"},
    {_in: "712811945", _out: "seven hundred and twelve million eight hundred and eleven thousand nine hundred and forty five"},
    {_in: "1001", _out: "one thousand and one"},
    {_in: "3,002", _out: "three thousand and two"},
    {_in: "4,022", _out: "four thousand and twenty two"},
    {_in: "-4,022", _out: "minus four thousand and twenty two"},
    {_in: "12000000000", _out: "twelve billion"},
    {_in: "12000000000000", _out: "twelve trillion"},
    {_in: "-999,456,789,012,345,678", _out: "minus nine hundred and ninety nine quadrillion four hundred and fifty six trillion seven hundred and eighty nine billion twelve million three hundred and forty five thousand six hundred and seventy eight"},
  ]

  test_numbers.each{|test_number|

    context "#{test_number[:_in]}" do
      it "should return #{test_number[:_out]}" do
        result = ArabicToEnglish.translate(test_number[:_in])
        expect(result).to eq test_number[:_out]
      end
    end

  }

end



















